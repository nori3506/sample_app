class AddIndexLikesUserId < ActiveRecord::Migration[5.1]
  def change
    add_index :likes, :user_id
    add_index :likes, :micropost_id
   
  end
end
