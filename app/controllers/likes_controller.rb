class LikesController < ApplicationController
  
  def create
    @micropost = Micropost.find(params[:micropost_id])
    @micropost.like(current_user)
    redirect_back(fallback_location: root_path)
  
  end
  
  def destroy
    @micropost = Like.find(params[:id]).micropost
    @micropost.dislike(current_user)
    redirect_back(fallback_location: root_path)  
  end


private 

  def like_params
    params.require(:like).permit(:user_id, :micropost_id)
  end
end